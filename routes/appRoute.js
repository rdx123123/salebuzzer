var Router= require('koa-router');
var bodyParser = require('koa-body')();

module.exports = function(app){
    var router = new Router();

    //Home Routes
    var homeCtrl = require('./../controllers/homeCtrl');
    router.get('/home', homeCtrl.showHomePage);
    router.get('/logout', homeCtrl.logout);
    //contest Routes
    var contestCtrl = require('./../controllers/contestCtrl');
    router.get('/contests/:status', contestCtrl.showContestsPage);
    router.get('/:slug/contest/:eid', contestCtrl.showContestDetailPage);
    router.post('/newContest',contestCtrl.showNewContestForm);
    router.post('/newContest/photo', contestCtrl.addNewPhotoContest);
    router.post('/newContest/spinwheel', contestCtrl.addNewSpinWheel);
    router.post('/livecontest', contestCtrl.contestuplode);
    router.post('/likecount', contestCtrl.likeCount);
    router.post('/closeContest/:eid', contestCtrl.closeContest);
    //Vendor dashboard
    var dashboardCtrl=require('./../controllers/dashboardCtrl');
    router.get('/dashboard/:vid',dashboardCtrl.showDashboard);
    router.get('/dashboard/:vid/allparticipants',dashboardCtrl.showallparticipants);
    router.get('/dashboard/coupon',dashboardCtrl.showcoupon);
    //User Profiles
    var userCtrl=require('./../controllers/userCtrl');
    router.get('/profile/:uid',userCtrl.showUserProfile);
    router.get('/login',userCtrl.showLoginPage);
    router.post('/login',userCtrl.login);
    router.get('/signup', userCtrl.showform);
    router.post('/signup', userCtrl.signup);
    router.get('/registerShop/:uid', userCtrl.showShopRegisterForm);
    router.post('/registerShop', userCtrl.registerShop);
    //spinwheel
    var WheelCtrl = require('./../controllers/WheelCtrl');
    router.get('/:slug/spinwheel/:cid', WheelCtrl.showSpinWheel);

    return router.middleware();
}