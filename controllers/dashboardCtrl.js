var sessionUtils = require('../utils/sessionUtils');
var util = require('util');
var databaseUtil = require('../utils/databaseUtils');
var voucher_code = require('../node_modules/voucher-code-generator/voucher_codes');

module.exports = {
  showDashboard: function* (next) {
    var vendorId = this.params.vid;
    var query = 'select user_id from vendor_profile where id="%s"';
    var userQuery = util.format(query, vendorId);
    var user = yield databaseUtil.executeQuery(userQuery);
    if (user[0] !== undefined) {
      var user_id = user[0].user_id;
      var showFullDetails = false;
      if ((this.currentUser) && (user_id == this.currentUser.id)) {
        showFullDetails = true;
      }
      query = 'select vendor_profile.name, vendor_profile.shopNo, vendor_profile.street, vendor_profile.locality, vendor_profile.city, vendor_profile.img, vendor_profile.gstn, user.email, user.mobile_no, user.img as dp from vendor_profile inner join user on vendor_profile.user_id=user.id where vendor_profile.id="%s"';
      var merchantQuery = util.format(query, vendorId);
      query = 'select event.title,event.id,event.slug, count(distinct user_id) as PARTICIPANTS from event inner join user_entity_upload on user_entity_upload.event_id=event.id where vendor_id="%s" group by event.id order by start_date desc';
      var eventQuery = util.format(query, vendorId);
      query = 'select first_name,last_name,mobile_no,email from user inner join user_entity_upload on user_entity_upload.user_id=user.id inner join event on event.id=user_entity_upload.event_id where vendor_id="%s"';
      var participantQuery = util.format(query, vendorId);
      var qs = 'select id,name from event_type';
      var typeResult = yield databaseUtil.executeQuery(qs);
      var merchantResult = yield databaseUtil.executeQuery(merchantQuery);
      var eventResult = yield databaseUtil.executeQuery(eventQuery);
      var participants = yield databaseUtil.executeQuery(participantQuery);

      query=util.format('select event.id,event.title,event.slug from event where vendor_id="%s"',vendorId);
      var contestList=yield databaseUtil.executeQuery(query);

      var voucher = voucher_code.generate({
        prefix: "promo-",
        postfix: "-2018",
        length: 8,
        count: 10
      });
      console.log(voucher);
      yield this.render('dashboard', {
        eventTypeList: typeResult,
        merchantDetails: merchantResult[0],
        eventList: eventResult,
        contestList:contestList,
        participants: participants,
        vendor_id: vendorId,
        showFullDetails: showFullDetails
      });
    }
  },
  showallparticipants: function* (next) {
    var vendorId = this.params.vid;
    var query = 'select first_name,last_name,mobile_no,email,title from user inner join user_entity_upload on user_entity_upload.user_id=user.id inner join event on event.id=user_entity_upload.event_id where vendor_id="%s"';
    var allparticipants = util.format(query, vendorId);
    var Result = yield databaseUtil.executeQuery(allparticipants);
    query = util.format('select name,img from vendor_profile where id="%s"', vendorId);
    var vendor = yield databaseUtil.executeQuery(query);
    yield this.render('allparticipants', {
      userDetails: Result,
      vendorDetails: vendor[0],
    });
  },
  showcoupon: function* (next) {
    yield this.render('coupon',{
    });
  }
}
