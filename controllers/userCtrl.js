var sessionUtils = require('../utils/sessionUtils');
var util = require('util');
var databaseUtils = require('./../utils/databaseUtils');

module.exports = {
    showUserProfile: function* (next) {
        var id = this.params.uid;
        console.log(id);
        var showFullDetails = false;
        if ((this.currentUser) && (id == this.currentUser.id)) {
            showFullDetails = true;
        }
        console.log(showFullDetails);
        var Query1 = 'select * from user where id="%s"';
        var userNameQuery = util.format(Query1, id);
        var result1 = yield databaseUtils.executeQuery(userNameQuery);
        console.log(result1[0]);
        var userDetails = result1[0];
        if (userDetails !== []) {
            var Query2 = 'select title,slug,start_date,end_date from event inner join user_entity_upload on user_entity_upload.event_id=event.id where user_entity_upload.user_id="%s" order by start_date desc';
            var historyQuery = util.format(Query2, id);
            console.log(historyQuery);

            var result2 = yield databaseUtils.executeQuery(historyQuery);

            console.log(userDetails);
            yield this.render('profile_user', {
                userDetails: userDetails,
                eventList: result2,
                showFullDetails: showFullDetails
            });
        }
    },
    login: function* (next) {
        var username = this.request.body.username;
        var password = this.request.body.password;
        console.log(username, password);
        var error;
        var query = util.format('select * from user where email="%s" and password="%s"', username, password);
        var result = yield databaseUtils.executeQuery(query);
        var user = result[0];
        // console.log('user',user);
        // console.log('vid:',result2);
        if (result.length > 0) {
            query = util.format('select id as vid from vendor_profile where user_id="%s"', user.id);
            var result2 = yield databaseUtils.executeQuery(query);
            var vid = [];
            for (var i = 0; i < result2.length; i++) {
                vid.push(result2[i].vid);
            }
            if (vid.length > 0)
                user.vid = vid;
            console.log(user);
            sessionUtils.saveUserInSession(user, this.cookies);
            this.redirect('/app/home');
        }
        else {
            error = "Wrong email or password";
            yield this.render('Login', {
                error: error,
            });
        }
    },

    showLoginPage: function* (next) {
        var error;
        yield this.render('Login', {
            error: error,
        });
    },

    signup: function* (next) {
        console.log('signup m aya data');
        var first_name = this.request.body.fields.first_name;
        var last_name = this.request.body.fields.last_name;
        var email = this.request.body.fields.email;
        var mobile_no = this.request.body.fields.mobile_no;
        var dob = this.request.body.fields.dob;
        var gender = this.request.body.fields.gender;
        var house_no = this.request.body.fields.house_no;
        var street = this.request.body.fields.street;
        var city = this.request.body.fields.city;
        var locality = this.request.body.fields.locality;
        var state = this.request.body.fields.state;
        var pincode = this.request.body.fields.pincode;
        var password = this.request.body.fields.psw;

        var userUploadedFile = this.request.body.files;
        var pic = userUploadedFile.userUploadedFile.path.split('/');
        console.log(pic[3]);

        var queryString = 'insert into user(first_name,last_name,email,mobile_no,dob,gender,house_no,street,city,locality,state,pincode,password,img) values("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")';
        var query = util.format(queryString, first_name, last_name, email, mobile_no, dob, gender, house_no, street, city, locality, state, pincode, password,pic[3]);

        var errorMessage;

        try {
            console.log('yaha aya');
            var result = yield databaseUtils.executeQuery(query);
            queryString = 'select * from user where email=%s';
            query = util.format(queryString, result.insertId);

            var insertedGuest = result[0];
            sessionUtils.saveUserInSession(insertedGuest, this.cookies);
        } catch (e) {
            if (e.code === 'ER_DUP_ENTRY') {
                errorMessage = 'Guest already exists';
            }
            else {
                throw e;
            }
        }
        if (errorMessage) {
            yield this.render('form', {
                errorMessage: errorMessage
            });
        }
        else {
            this.redirect('/app/home');
        }
    },

    showform: function* (next) {
        console.log('yahaaya showform');
        yield this.render('form', {
        });
    },
    showShopRegisterForm: function* (next) {
        var user_id = this.params.uid;
        console.log('yahaaya showShopRegisterForm');
        yield this.render('shopRegisterForm', {
            user_id: user_id
        });
    },
    registerShop: function* (next) {
        console.log('yahaaya shop register submit');
        var qs = 'select id from firm_domain where name="%s"';
        var query = util.format(qs, this.request.body.fields.firm_domain);
        var result = yield databaseUtils.executeQuery(query);
        var domain_id = result[0].id;
        console.log('domain_id', domain_id);

        
        var userUploadedFile = this.request.body.files;
        var pic = userUploadedFile.userUploadedFile.path.split('/');
        console.log(pic[3]);

        qs = 'insert into vendor_profile (name,domain_id,user_id,gstn,shopNo,street,locality,city,img) values ("%s","%s","%s","%s","%s","%s","%s","%s","%s")';
        query = util.format(qs, this.request.body.fields.name, domain_id, this.request.body.fields.user_id, this.request.body.fields.gstn, this.request.body.fields.shopNo, this.request.body.fields.street, this.request.body.fields.locality, this.request.body.fields.city,pic[3]);
        result = yield databaseUtils.executeQuery(query);
        console.log('vendor_id:', result.insertId);

        if(this.currentUser.vid)
            this.currentUser.vid.push(result.insertId);
        else
            this.currentUser.vid=[result.insertId];

        sessionUtils.saveUserInSession(this.currentUser, this.cookies);

        this.redirect('/app/home');
    }

}
