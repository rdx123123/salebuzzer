var sessionUtils = require('../utils/sessionUtils');
var util = require('util');
var databaseUtils = require('./../utils/databaseUtils');

module.exports = {
  showContestDetailPage: function* (next) {
    var eventId = this.params.eid;
    var queryString = 'select event_type_id, event.id as vid, title, vendor_profile.name, vendor_profile.id as id, description, prize_description, start_date, end_date, total_prizes, prize_validity from event inner join vendor_profile on event.vendor_id=vendor_profile.id where event.id="%s"';
    var query = util.format(queryString, eventId);
    var result1 = yield databaseUtils.executeQuery(query);
    queryString = 'select user_entity_upload.id, entity_file_url,user.first_name,user.last_name from user_entity_upload inner join user on user.id= user_entity_upload.user_id where user_entity_upload.event_id="%s"';
    query = util.format(queryString, eventId);
    var result2 = yield databaseUtils.executeQuery(query);
    queryString = 'select count(user_entity_upload.id) as no_of_submissions, count(distinct user_id) as no_of_users from user_entity_upload where event_id="%s"';
    query = util.format(queryString, eventId);
    var result3 = yield databaseUtils.executeQuery(query);
    var r = yield databaseUtils.executeQuery(util.format('select user_entity_upload.id,entity_file_url as pic  from user_entity_upload '));
    var like = yield databaseUtils.executeQuery(util.format('SELECT user_entity_upload_id,COUNT(user_id) as c FROM user_entity_like GROUP BY user_entity_upload_id order by c desc'));
    var showFullDetails = false;
    if ((this.currentUser) && (result1[0].id == this.currentUser.id)) {
      showFullDetails = true;
    }
    yield this.render('contest', {
      contestDetails: result1[0],
      participant_list: result2,
      userNumber: result3[0],
      eid: eventId,
      r: r,
      like: like,
      showFullDetails: showFullDetails
    });
  },
  showContestsPage: function* (next) {
    var status = this.params.status;
    var qs = "";
    if (status === 'live') {
      qs = 'select event.id, title, vendor_profile.name, slug, start_date, end_date, total_prizes from event join vendor_profile on event.vendor_id=vendor_profile.id where current_timestamp < end_date and current_timestamp >= start_date order by end_date';
    }
    else if (status === 'upcoming') {
      qs = 'select event.id, title, vendor_profile.name, slug, start_date, end_date, total_prizes from event join vendor_profile on event.vendor_id=vendor_profile.id where current_timestamp<start_date order by start_date';
    }
    var result = yield databaseUtils.executeQuery(qs);
    yield this.render('contest_list', {
      contest_list: result,
    });
  },
  contestuplode: function* (next) {
    var id = '/app/slug/contest/' + this.request.body.fields.eid;
    var eventId = this.request.body.fields.eid;
    console.log(id);
    var cu = this.currentUser;
    console.log(cu);
    var userUploadedFile = this.request.body.files;
    var pic = userUploadedFile.userUploadedFile.path.split('/');
    console.log(pic);
    console.log(cu.id, parseInt(eventId));
    var entity_type_id = 1;//for photocontest
    var qurery = yield databaseUtils.executeQuery(util.format('insert into user_entity_upload(entity_type_id,user_id,event_id,entity_file_url) \
          values("%s","%s","%s","%s")', entity_type_id, cu.id, parseInt(eventId), pic[3]));
    this.redirect(id);
  },
  likeCount: function* (next) {
    var id = '/app/slug/contest/' + this.request.body.eid;
    console.log(this.request.body.eid);
    var like = this.request.body.like;
    console.log(like, this.currentUser.id);
    var r = yield databaseUtils.executeQuery(util.format('SELECT * FROM user_entity_like WHERE user_entity_upload_id=%s and USER_ID=%s', parseInt(like), parseInt(this.currentUser.id)));
    if (r.length == 0) {

      r = yield databaseUtils.executeQuery(util.format('INSERT INTO user_entity_like (user_entity_upload_id,USER_ID) VALUES(%s,%s)', parseInt(like), parseInt(this.currentUser.id)));
    }
    else {
      r = yield databaseUtils.executeQuery(util.format('DELETE FROM user_entity_like  WHERE user_entity_upload_id=%s and USER_ID=%s', parseInt(like), parseInt(this.currentUser.id)));

    }
    this.redirect(id);
  },
  showNewContestForm: function* (next) {
    var vendor_id = this.request.body.vendor_id;
    var event_type = this.request.body.event_type;
    var qs = util.format('select id,name from event_type where id="%s"', event_type);
    var result = yield databaseUtils.executeQuery(qs);
    var type = result[0];
    if (type.name === 'photo Event') {
      yield this.render('photoContestForm', {
        event_type_id: type.id,
        vendor_id: vendor_id
      });
    } else if (type.name === 'spinwheel') {
      yield this.render('wheel_form', {
        event_type_id: type.id,
        vendor_id: vendor_id
      });
    }
  },
  addNewPhotoContest: function* (next) {
    var moderator_required = this.request.body.moderator_required ? 1 : 0;
    var slug = this.request.body.title + "-" + this.request.body.vendor_id + "-" + this.request.body.event_type;
    var qs = 'insert into event(title,event_type_id,vendor_id,start_date,end_date,total_prizes,moderator_required,description,prize_description,prize_validity,slug) values("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")';
    var query = util.format(qs, this.request.body.title, this.request.body.event_type_id, this.request.body.vendor_id, this.request.body.start_date, this.request.body.end_date, this.request.body.total_prizes, moderator_required, this.request.body.description, this.request.body.prize_description, this.request.body.prize_validity, slug);
    var result = yield databaseUtils.executeQuery(query);
    qs = 'select id from event where slug="%s"';
    query = util.format(qs, slug);
    var c_id = yield databaseUtils.executeQuery(query);
    yield this.render('redirect', {
      slug: slug,
      id: c_id[0].id
    });
  },
  addNewSpinWheel: function* (next) {
    var slug = this.request.body.title + "-" + this.request.body.vendor_id + "-" + "spinwheel";
    var event_type_id = 6;

    var qs = 'insert into event(title,Event_type_id,vendor_id,start_date,end_date,total_prizes,description,prize_validity,slug) values("%s",6,"%s","%s","%s","%s","%s","%s","%s")';

    var query = util.format(qs, this.request.body.title, this.request.body.vendor_id, this.request.body.start_date, this.request.body.end_date, this.request.body.sectors, this.request.body.description, this.request.body.prize_validity, slug);

    var result = yield databaseUtils.executeQuery(query);
    var event_id = result.insertId;

    qs = 'insert into spinwheel_offer (event_id, sector_data) values ("%s","%s")';
    for (var i = 0; i < this.request.body.offer.length; i++) {
      query = util.format(qs, event_id, this.request.body.offer[i]);
      result = yield databaseUtils.executeQuery(query);
    }
    var redirectUrl = '/app/' + slug + '/contest/' + event_id;
    this.redirect(redirectUrl);
  },
  closeContest: function* (next) {

    var eventId = this.params.eid;
    var queryString = 'update event set end_date=now() where id="%s"';
    var query = util.format(queryString, eventId);
    var result = yield databaseUtils.executeQuery(query);

    yield this.render('ended', {});

  }
}
