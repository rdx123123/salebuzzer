var sessionUtils = require('../utils/sessionUtils');
var util = require('util');
var databaseUtils = require('./../utils/databaseUtils');

module.exports = {

	showHomePage: function* (next) {

		var queryString1 = 'select event.id, vendor_id, title, vendor_profile.name, slug, start_date, end_date from event join vendor_profile on event.vendor_id= vendor_profile.id where current_timestamp<end_date and current_timestamp>=start_date order by end_date limit 4';

		var queryString2 = 'select event.id, vendor_id, title, vendor_profile.name, slug, start_date, end_date from event join vendor_profile on event.vendor_id=vendor_profile.id where current_timestamp<start_date order by start_date limit 4';

		var queryString3 = 'select substring_index(group_concat(Id order by score desc),",",1) as Id, substring_index(group_concat(img order by score desc),",",1) as img, title, max(score) as points, substring_index(group_concat(first_name order by score desc),",",1) as name from ( select event_id, title, user.id as Id, first_name, count(*) as score, user.img from user_entity_upload inner join user_entity_like on user_entity_upload.id= user_entity_upload_id inner join event on event_id=event.id inner join user on user_entity_upload.user_id=user.id inner join vendor_profile on vendor_profile.id=event.vendor_id group by user_entity_upload_id) as users group by event_id order by event_id desc limit 4';

		var result1 = yield databaseUtils.executeQuery(queryString1);
		var result2 = yield databaseUtils.executeQuery(queryString2);
		var result3 = yield databaseUtils.executeQuery(queryString3);
		//var userDetails=result[0];

		yield this.render('home', {
			liveEventList: result1,
			upcomingList: result2,
			winnerList: result3
		});
	},

	logout: function* (next) {
		var sessionId = this.cookies.get("SESSION_ID");
		if (sessionId) {
			sessionUtils.deleteSession(sessionId);
		}
		this.cookies.set("SESSION_ID", '', { expires: new Date(1), path: '/' });

		this.redirect('/app/home');
	}
}
