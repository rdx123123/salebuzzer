var sessionUtils = require('../utils/sessionUtils');
var util= require('util');
var databaseUtils= require('./../utils/databaseUtils');

module.exports = {
    showSpinWheel: function* (next) {
        var qs = 'select sector_data from spinwheel_offer where event_id="%s"';
        var query=util.format(qs,this.params.cid);
        var spin =  yield databaseUtils.executeQuery(query);
        qs='select name from vendor_profile join event on event.vendor_id=vendor_profile.id where event.id="%s"';
        query=util.format(qs,this.params.cid);
        var vendor_name=yield databaseUtils.executeQuery(query);
	    yield this.render('wheel',{
            spinWheel:spin,
            vendor_name:vendor_name
        });
    }
}
